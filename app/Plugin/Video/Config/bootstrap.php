<?php

MooCache::getInstance()->setCache('video', array('groups' => array('video')));

if (Configure::read('Video.video_enabled')) {
    App::uses('VideoListener', 'Video.Lib');
    CakeEventManager::instance()->attach(new VideoListener());
    
    MooSeo::getInstance()->addSitemapEntity("Video", array(
    	'video'
    ));
}