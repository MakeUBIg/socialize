<div id="footer">
    <?php echo html_entity_decode( Configure::read('core.footer_code') )?>
    <?php if (Configure::read('core.show_credit')): ?>
    <span class="date"><?php echo __('Developed by')?> <a href="http://makeubig.com/" target="_blank">MakeUBIG <?php echo Configure::read('core.version')?></a></span>
    <?php endif; ?>
</div>