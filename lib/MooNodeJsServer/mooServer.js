// Setup basic express server

var express = require('express');
var app = express();
var server = require('http').createServer(app);
// For https
/*
var fs = require('fs');
var privateKey = fs.readFileSync( 'privatekey.pem' );
var certificate = fs.readFileSync( 'certificate.pem');

var server = require('https').createServer({
    key: privateKey,
    cert: certificate
}, app).listen(3000);
*/
var io = require('socket.io')(server);
var port = process.env.PORT || 3000;

var mooEmitter = require('./mooEmitter');
var mooSocket = require('./mooSocket');
var mooNotification = require('./mooNotification');
mooNotification.setIO(io);

var auth = require("./mooAuth");
var user = require('./mooUser');
var log = require("./mooLog");


require('./mooConfig').getMooConfig(function (host, login, password, database, prefix, sourceSql, mysqlPort,salt) {
    // Setup moosocial integration
    require("./mooDB").config(host, login, password, database, prefix, sourceSql, mysqlPort);
    require("./mooBooting").run();
    mooEmitter.emit('mooConfigSuccess',host, login, password, database, prefix, sourceSql, mysqlPort,io,salt);
    server.listen(port, function () {

        log.info('Server listening at port %d', port);

    });

// Routing
    app.use(express.static(__dirname + '/public'));


    io.on('connection', function (socket) {
        // Hacking for all "on" event to make sure the socked is authenticated
        socket.isLogged = false;
        socket.userId = 0;
        socket.myFriendsId = [];
        socket.myBlockersId = [];
        socket.roomsId = {actived: []};
        var onevent = socket.onevent;
        socket.onevent = function (packet) {
            
            if (socket.isLogged) {
                onevent.call(this, packet);
            }else{
                if(packet.hasOwnProperty('data')){
                    if (packet.data instanceof Array) {
                        
                        if(packet.data.length > 0){
                            switch(packet.data[0]) {
                                case 'getServerInfo':
                                case 'getMonitorMessages':
                                case 'getUsers':
                                case 'getRooms':    
                                    onevent.call(this, packet);
                                    break;
                                default:

                            }
                        }
                    }



                }
            }

        };
        // End hacking

        auth.execute(socket);

        user.init(io);
        mooEmitter.emit('io_connection',io,socket);

        // User init
        socket.on("getMyFriendsOnline", function () {
            user.getMyFriendsOnline(this);
        });
        socket.on("getMyFriends", function (ids) {
            user.getMyFriends(this,ids);
        });
        socket.on("getUsers", function (ids) {
            user.getUsers(this,ids);
        });
        socket.on("getUsersByRoomIdsAtBooting", function (rIds) {
            user.getUsersInRooms(this,rIds);
        });

        socket.on("setOffline", function () {
            user.setOffline(this);
        });
        socket.on("setOnline", function () {
            user.setOnline(this);
        });
        socket.on("getMyGroups", function () {
            user.getMyGroupsConversations(this);
        });
        socket.on("startTyping", function (rId) {
            user.startTyping(this,rId);
        });
        socket.on("stopTyping", function (rId) {
            user.stopTyping(this,rId);
        });
        socket.on("searchFriend", function (name) {
            user.searchFriend(this,name);
        });
        // End user init

        // when` the user disconnects.. perform this
        socket.on('disconnect', function () {
            mooEmitter.emit('io_disconnect',io,socket);

            var userId = socket.userId;
            
            mooSocket.sub1FromNumberUsersSocket(userId);
            setTimeout(function () {

                if (mooSocket.isUserLatestConnecting(socket)) {
                    mooNotification.imOffline(userId);
                    // Hacking for display all users
                    //user.imOffline(userId);
                    // End hacking
                }
            }, 1100);
        });
    });
});


