var chatDB = require("./chatDB");

var chatSocket = require('./chatSocket');

var log = require("../../mooLog");
var cache = require("./chatCache");
var _io;
var _salt = "moo";
var _ = require('lodash');

var jwt = require('jsonwebtoken');

var ChatVideo = (function () {

    var init = function (io,salt) {
        _io = io;
        _salt = salt;
    };
    var calling = function (socket,token) {
        var decoded = jwt.verify(token, _salt);
        var members = _.get(decoded,"members",[]);
        if( members.length > 0){
            _.forEach(members, function(uId) {
                _io.to('mooUser.' + uId).emit('videoCallingCallback',{
                    rId:_.get(decoded,"rId",0),
                    members:members,
                    senderId:socket.userId,
                    senderSocketId:socket.id
                });
            });
        }

    };
    var token = function(socket,obj){
        var token = jwt.sign(obj, _salt);
        socket.emit("getVideoCallingTokenCallback", token);
    };
    /**
     *
     * @param socket
     * @param obj {
     *                  receiverId: socket id of caller,
     *                  signal: data of signal for creating connect peer-to-peer
     *            }
     */
    var sendSignal = function (socket,obj) {
        if(_.has(obj,'receiverSocketId')){
            _.assignIn(obj,{senderSocketId:socket.id});
            console.log(obj);
            socket.to(_.get(obj,'receiverSocketId')).emit("receiveSignal",obj);
        }
    }
    return {
        init: init,
        calling:calling,
        token:token,
        sendSignal:sendSignal,
    };
}());


module.exports = ChatVideo;