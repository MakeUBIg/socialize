//var mooEmitter = require('../../../../../../lib/MooNodeJsServer/mooEmitter.js');
var mooEmitter = require('../../mooEmitter');
var message  = require("./chatMessage");
var video    = require("./chatVideo");
var chatInfo = require('./chatInfo');

var mooChat = (function () {

    var init = function(){
        mooEmitter.on('mooConfigSuccess',function(host, login, password, database, prefix, sourceSql, mysqlPort,io,salt){
            require("./chatDB").config(host, login, password, database, prefix, sourceSql, mysqlPort);
            message.init(io);
            video.init(io,salt);
        });

        mooEmitter.on('io_connection',function(io,socket){
            // Server info
            socket.on('getServerInfo',function(){
                chatInfo.getServerInfo(this);
            });
            socket.on('getMonitorMessages',function(limit){
                chatInfo.getMonitorMessages(this,limit);
            });
            socket.on('getRooms',function(ids){
                chatInfo.getRooms(this,ids);
            });
            // End server info
            // Message init
            socket.on("saveRoomStatus", function (data) {
                message.saveRoomStatus(this,data.rooms);
            });
            socket.on("createChatWindowByUser", function (data) {
                message.createRoom(this,data.friendIds,data.isAllowedSendToNonFriend);
            });
            socket.on("createChatWindowByRoomId", function (data) {
                message.createChatWindowByRoomId(this,data);
            });
            socket.on("refeshStatusChatWindowByRoomId", function (rId) {
                message.refeshStatusARoom(this,rId);
            });
            socket.on("sendTextMessage", function (data) {
                message.setTextMessage(this,data);
            });
            socket.on("getRoomMessages", function (data) {
                message.getRoomMessages(this,data.roomId,data.limit,data.firstIdNewMessage);
            });
            socket.on("getRoomMessagesMore", function (data) {
                message.getRoomMessagesMore(this,data.rId,data.mIdStart,data.limit);
            });
            socket.on("markMessagesIsSeenInRooms", function (data) {
                message.markMessagesIsSeenInRooms(this,data.messageIdsUnSeen,data.roomIsSeen);
            });
            socket.on("getRoomHasUnreadMessage", function () {
                message.getRoomHasUnreadMessage(this);
            });
            socket.on("deleteConversation", function (rId) {
                message.deleteConversation(this,rId);
            });
            socket.on("reportMessageSpam", function (data) {
                message.reportMessageSpam(this,data);
            });
            socket.on("leaveConversation", function (rId) {
                message.leaveConversation(this,rId);
            });
            socket.on("addUsersToARoom", function (friendIds, roomId) {
                message.addUsersToARoom(this,friendIds, roomId);
            });
            socket.on("blockMessages", function (rId) {
                message.blockMessages(this, rId);
            });
            socket.on("unblockMessages", function (rId) {
                message.unblockMessages(this,rId);
            });
            // End message init
            // Video call init
            socket.on("videoCalling", function (token) {
                video.calling(this,token);
            });
            socket.on("getVideoCallingToken",function(obj){
                video.token(this,obj);
            });
            socket.on("sendSignal",function(obj){
                video.sendSignal(this,obj)
            });
            // End video call init
        });
    };


    return {
        init:init
    };
}());
module.exports =  mooChat ;