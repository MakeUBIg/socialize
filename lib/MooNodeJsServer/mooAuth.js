//var mooDB;
var mooDB = require("./mooDB");
var mooSocket = require('./mooSocket');
var mooNotification = require('./mooNotification');
var mooUser = require('./mooUser');
var cache = require("./mooCache");
var log = require("./mooLog");

var events = require('events');

function mooAuth() {
    events.EventEmitter.call(this);

    var initDataSource = function (mooDB) {
        mooDB = mooDB;

    };
    var isLogged = function (socket, callback) {
        if (socket.isLogged) {
            callback(socket);
        }
        return socket.isLogged;
    };
    var id = function (socket) {
        return socket.userId;
    };
    var execute = function (socket) {

        socket.isLogged = false;
        socket.userId = 0;
        // Check token is valided

        if ((socket.handshake.query.chat_token)) {
            mooDB.query(mooDB.mysql.format(mooDB.sqlString.checkTokenIsExists, [socket.handshake.query.chat_token]), function (err, rows) {
                if (err) {
                    //io.emit('error');
                } else {
                   
                    if (rows.length == 0) {
                        socket.emit("userIsLogged", 0);
                    } else {
                        socket.isLogged = true;
                        socket.userId = rows[0].user_id;
                        socket.join('mooUser.' + socket.userId);
                        mooUser.setStatus(socket.userId, socket.handshake.query.chat_status);
                        
                        if (mooSocket.isUserFirstTimeConnecting(socket.userId) && !mooUser.isOffline(socket.userId)) {
                            mooNotification.imOnline(socket.userId);
                            // Hacking for display all users
                            //mooUser.imOnline(socket.userId)
                            // End hacking
                        }
                        mooNotification.imLogged(socket);
                        mooSocket.add1ToNumberUsersSocket(socket.userId); 
                        mooDB.query(mooDB.mysql.format(mooDB.sqlString.getMyStatCached, [socket.userId]), function (err, rows) {
                            if (err) {
                                log.error("mooDB.sqlString.getMyStatCached", err);
                            } else {
                                if (rows.length == 0) {

                                }else{

                                    if(rows[0].new_friend == 1 || rows[0].new_block == 1 || rows[0].new_profile == 1){
                                        cache.emptyQuery(socket.userId);
                                        mooDB.query(mooDB.mysql.format(mooDB.sqlString.setMyStatCached, [socket.userId]), function (err, rows) {
                                            if (err) {
                                                log.error("mooDB.sqlString.setMyStatCached", err);
                                            } else {

                                            }}
                                        );
                                        // Hacking for 'Do not show my online status'
                                        
                                        if(rows[0].new_profile == 1){
                                            mooUser.updateHideMyOnlineStatus(socket.userId,true);
                                        }
                                    }


                                }
                            }
                            
                            mooSocket.initFriendsAndBlocker(socket);
                            mooUser.updateHideMyOnlineStatus(socket.userId,false);
                        });

                    }
                }
            });
        } else {

        }


    };
    return {
        initDataSource: initDataSource,
        isLogged: isLogged,
        id: id,
        execute: execute,
        mooDB: mooDB
    };
};

module.exports = new mooAuth();